<?php 

    // TAG COMMENT: PROGRESS 100%
    // CONTRIBUTOR: BACKEND - TAUFAN

//Declare variable to prevent error
$fname = "";            // first name
$lname = "";            // last name
$em = "";               // email
$em2 = "";              // email 2
$password = "";         // password
$password2 = "";        // password 2
$date = "";             // sign up date
$error_array = array(); // holds error messages

if(isset($_POST['register_button']))
{
    // Registration form values

    // fname
    $fname = strip_tags($_POST['reg_fname']);   // remove html tags
    $fname = str_replace(' ', '', $fname);      // remove spaces
    $fname = ucfirst(strtolower($fname));       // uppercase first letter
    $_SESSION['reg_fname'] = $fname;            // store input into session variable

    // lname
    $lname = strip_tags($_POST['reg_lname']);
    $lname = str_replace(' ', '', $lname);
    $lname = ucfirst(strtolower($lname));
    $_SESSION['reg_lname'] = $lname;
    
    // email
    $em = strip_tags($_POST['reg_email']);
    $em = str_replace(' ', '', $em);
    $_SESSION['reg_email'] = $em;

    // email2
    $em2 = strip_tags($_POST['reg_email2']);
    $em2 = str_replace(' ', '', $em2);
    $_SESSION['reg_email2'] = $em2;

    // password
    $password = strip_tags($_POST['reg_password']);
    $password2 = strip_tags($_POST['reg_password2']);

    $date = date("Y-m-d"); // get current date

    if($em == $em2)
    {
        // check if email in valid format
        if(filter_var($em, FILTER_VALIDATE_EMAIL))
        {
            $em = filter_var($em, FILTER_VALIDATE_EMAIL);
            
            // check if email already exist
            $e_check = mysqli_query($con, "SELECT email FROM users WHERE email='$em'");
        
            //  count the number of rows returned
            $num_rows = mysqli_num_rows($e_check);
            
            if($num_rows > 0)
            {
                array_push($error_array, "Email already in use.<br>");
            }

        }
        else
        {
            array_push($error_array, "Invalid email format.<br>");            
        }
    }
    else
    {
        array_push($error_array, "Email do not match.<br>");        
    }

    // validate first name and last name    
    if(strlen($fname) < 2 || strlen($fname) > 25)
    {
        array_push($error_array, "Your first name must be between 2 and 25 characters.<br>");        
    }

    if(strlen($lname) <= 2 || strlen($lname) >= 25)
    {
        array_push($error_array, "Your last name must be between 2 and 25 characters.<br>");        
        
    }

    // validate password
    if($password != $password2)
    {
        array_push($error_array, "Password do not match.<br>");                
    }
    else
    {
        if(preg_match('/[^A-Za-z0-9]/', $password))
        {
            array_push($error_array, "Your password can only contain english character or number.<br>");                            
        }
    }

    if(strlen($password) <= 5 || strlen($password) >= 30)
    {
        array_push($error_array, "Your password must be between 5 or 30 character.<br>");
    }

    if(empty($error_array))
    {
        $password = md5($password); // encrypt password before insert it to database
        
        // generate username by contenating first name and last name
        $username = strtolower($fname . "_" . $lname);
        $check_username_query = mysqli_query($con, "SELECT username FROM users WHERE username = '$username'");

        $i = 0;
        // if username exist add number to to username
        while(mysqli_num_rows($check_username_query) != 0)
        {
            $i++;
            $username = $username . "_" . $i;
            $check_username_query = mysqli_query($con, "SELECT username FROM users WHERE username = '$username'");
            
        }

        // profile picture assignment
        $profile_pic = "";
        $rand = rand(1, 3); // random number between 1 and 3
        switch($rand)
        {
            case 1:
                $profile_pic = "assets/images/profile_pics/default/head_alizarin.png";
                break;
            case 2:
                $profile_pic = "assets/images/profile_pics/default/head_deep_blue.png";
                break;
            case 3:
                $profile_pic = "assets/images/profile_pics/default/head_wet_asphalt.png";
                break;
            
        }

        $query = mysqli_query($con, "INSERT INTO users VALUES ('', '$fname', '$lname', '$username', '$em', '$password', '$date', '$profile_pic', '0', '0', 'no', ',')");

        array_push($error_array, "<span style='color: #14C800;'>You're all set! Go ahead login to Dashblue!</span><br>");

        // clear session variable
        $_SESSION['reg_fname'] = "";
        $_SESSION['reg_lname'] = "";
        $_SESSION['reg_email'] = "";
        $_SESSION['reg_email2'] = "";

        
        
    }

}
?>