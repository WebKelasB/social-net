
database "social" phpmyadmin
===========
table "users" AUTO INCREMENT CHECKED
- id PK
- first_name varchar(25)
- last_name varchar(25)
- username varchar(25)
- email varchar(50)
- password varchar(255)
- signup_date date
- profile_pic varchar(255)
- num_posts int
- num_likes int
- user_closed varchar(3)
- friend_array text


table posts
- id PK auto increment
- body TEXT
- added_by VARCHAR 60
- user_to VARCHAR 60
- date_added DATETIME
- user_closed VARCHAR 3
- deleted VARCHAR 3
- likes

table likes
- id auto increment
- username VARCHAR 60
- post_id INT

table comments



=============
