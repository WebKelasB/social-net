kubukanansehatselalu

social-net



setelah clone repo gunakan branch "taufan-06508" untuk melajutkan project

NOTE:
branch "taufan-06508" merupakan **bleeding edge** yang mana akan menjadi cabang paling terupdate (diupdate setiap hari) sampai hari H pengumpulan tugas.

branch "master" branch **rolling release** diupdate berkala dan sebagai branch backup


PENTING:

1. buat cabang / branch dengan nama+npm (taufan-06508) yang digunakan untuk push ke gitlab

2. tugas bebas. aku (taufan) membuat susunan dasarnya (php/html/javascript/database). semua file yang aku push itu FIXED tapi masih "berantakan" file php + html jadi 1. klo mau misah file jadi .php sendiri, .html sendiri silahkan.

3. perhatikan TAG COMMENT 
    //TAG COMMENT: PROGRESS
    //TAG COMMENT: FINISH

    jika ingin mecah file (misah file .php / .html) pastikan sudah TAG COMMENT : FINISH
    BIAR GA BENTROK COMMIT MASTER

4. TAG COMMENT: CONTRIBUTOR
    harap klo menambakan / edit file berikan nama contributor dan apa yg dilakukan

    // CONTRIBUTOR: FRONTEND - Diki (CSS homepage)
    // CONTRIBUTOR: BACKEND - Taufan (logic+DB)


ROLE:
1. Frontend : ngatur tampilan (css/html)
2. Backend : ngatur jalannya logic web (php/js/database)

note: backend tidak harus murni buat dari 0. yg mau jadi backend tp belum bisa banyak silahkan rapikan codingan yang sudah ada. mecah program jadi beberap fungsi dll.

aku buat pake teknik **defensive programming** (google klo ga tau). 
jadi program (logic backend) web asal jalan. tp struktur kodingan ndak rapi. jadi yg mau kontribusi backend silahkan rapikan codingan yg sudah ada.

Backend Koordinator: Taufan Augusta <--- Hardcore Backend mikir dari 0

Frontend Koordinator: Diki Adita Mara <--- Mastah CSS


 
TO DO:
1. REGISTER FORM - 100%
2. LOGIN FORM - 0%
3. HOME PAGE - 0%
4. POSTING LOGIC - 0%
5. MESSAGING LOGIC 0%