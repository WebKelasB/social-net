$(document).ready(function(){

    // ON CLICK SIGN UP, HIDE LOGIN AND SHOW REGISTRATION FORM
    $("#signup").click(function() {
        $("#first").slideUp("slow", function(){
             $("#second").slideDown("slow");
        });
    });

    // ON CLICK SIGN IN, HIDE REGISTRATION AND SHOW LOGIN FORM
    $("#signin").click(function() {
        $("#second").slideUp("slow", function(){
             $("#first").slideDown("slow");
        });
    });

});